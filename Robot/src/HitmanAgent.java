import robocode.*;


import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;


public class HitmanAgent extends AdvancedRobot {
    Random random = new Random();
    int count = 0;

    String target;
    boolean isally;
    double bulletspower;
    int antalbullets;


    ArrayList<String> enemy = new ArrayList<String>();



    public void run() {


        setColors(Color.black, Color.black, Color.RED, Color.RED, Color.black); // body,gun,radar,bulletColor,scanArcColor

        // Robot main loop

        while (getOthers() > 1) {

            setAhead(100 + random.nextInt(200));
            setTurnRight(50 + random.nextInt(200));
            setTurnLeft(100 + random.nextInt(200));
            turnGunLeft(360);
            setMaxVelocity(7); // faster you go slower turn


            execute();


        }
        while (getOthers() == 1) {

            setAhead(200);
            setTurnRight(300);
            setAhead(200);
            setTurnLeft(100);
            setMaxVelocity(8);
            turnGunLeft(180);


            execute();

        }

    }


    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        target = e.getName();


        isally = target.contains("Part4");
        if (isally) {
            out.println("be kind to him");

            fire(1);

        }


        if (getOthers() > 1) {
            if (e.getDistance() < 100)
                fireBullet(2.5);
            if (e.getDistance() < 50)
                fireBullet(3);
            else
                fireBullet(2);
        }

        if (getOthers() > 6 || getEnergy() > 100)
            fireBullet(3);


        // fire with low fire to save power because its hard to hit enemy
        if (getOthers() == 1 && getEnergy() < 50) {
            fireBullet(1.5);
        }
        if (getOthers() == 1 && getEnergy() > 50) {
            fireBullet(3);
        }


    }

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        System.out.println(getEnergy());
        target = e.getName();
        bulletspower = e.getPower();
        antalbullets++;
        // damage can be from ramming ,missing bullets and hit by bullets.
        double bulletsdamage = 0; // just damage from bullet
        double total;
        total = bulletspower * 4;
        bulletsdamage = bulletsdamage + total;
        System.out.println(target + " hit me by a bullet \t and the power of it " + bulletspower);

        System.out.println("antal bullet is \t" + antalbullets);

        System.out.println("Total damage to me from bullets \t" + bulletsdamage);



    }

    // Miss the bullet
    @Override
    public void onBulletMissed(BulletMissedEvent e) {

        System.out.println("you missed the target");

    }

    // hit the bullet the enemy
    @Override
    public void onBulletHit(BulletHitEvent e) {

        System.out.println("we sucess hit the enamy" + e.getName());
    }

    // Hit Robot
    @Override
    public void onHitRobot(HitRobotEvent e) {
        setBack(150);
        setTurnLeft(50);
    }

    // Hit the wall
    @Override
    public void onHitWall(HitWallEvent e) {

        setBack(100 + random.nextInt(200));

    }

    @Override
    public void onBulletHitBullet(BulletHitBulletEvent e) {
        System.out.println("bullet hit another bullet");

    }

    @Override
    public void onRobotDeath(RobotDeathEvent e) {

        System.out.println(e.getName() + "is death");

    }

    // win behavior
    public void onWin(WinEvent e) {

        setBack(100);
        setTurnRight(300);

    }
}

/* Methods to use:
 *
 *   getEnergy() // return robots energy
 *   fireBullet() // fire bullet
 *   getGunHeat() //  Returns the current heat of the gun
 *   getNumRounds() // Returns the number of rounds in the current battle.
 *   doNothing() //  meaning that the robot will skip it's turn
 *   getX() and getY() Returns the X and Y position of the robot
 *   getName(); //Returns the robot's name.
 *   getHeading() //returns the direction that the robot's body is facing, in degrees
 *
 *
 *
 * */